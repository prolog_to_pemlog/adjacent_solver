#Access https://www.chiark.greenend.org.uk/~sgtatham/puzzles/js/unequal.html and generate a board, then copy url created by clicking "by Game ID" below the board
#URL Example https://www.chiark.greenend.org.uk/~sgtatham/puzzles/js/unequal.html#7:0,0,0,0R,0R,0,0,0UR,0D,0L,0,0L,0,0U,0D,0R,0,0,0RL,0,0U,0R,0U,0L,0R,0R,0,0U,0,0,0,0D,0L,0,0UD,0,0D,0,0,0L,0,0DL,0,0,7,0U,0,0U,0,
def printList(array):
    answer="["
    for i in array:
        acc="["
        for j in i:
            acc+=str(j)+","
        acc=acc[:-1]+"],"
        answer+=acc
    return( answer[:-1]+']')

x=input("URL : ")
x=x.split("chiark.greenend.org.uk/~sgtatham/puzzles/js/unequal.html#")[1][:-1] #Remove url and trailing comma
x=x.split(":")
length=int(x[0])
x=x[1].split(',')
array=[]

init=[['_' for i in range(length) ] for i in range(length)]
hConstraint=[['_' for i in range(length-1) ] for i in range(length)]
vConstraint=[['_' for i in range(length-1) ] for i in range(length)]
for i in range(length):
    array.append(x[i*length:(i+1)*length])

for i in range(len(array)):
    for j in range(len(array)):
        element = array[i][j]
        number = element[0]
        constraint = element[1:]
        if (not number=='0'):
            init[i][j]=int(number)
        #print(i,j,array[i][j])
        if(len(constraint)==1):
            if (constraint=="U"):
                vConstraint[j][i-1]='l'
            elif (constraint=="D"):
                vConstraint[j][i]='g'
            elif (constraint=="L"):
                hConstraint[i][j-1]='l'
            elif (constraint=="R"):
                hConstraint[i][j]='g'
        elif(len(constraint)==2):
            constraint2=constraint[1]
            constraint=constraint[0]
            if (constraint=="U"):
                vConstraint[j][i-1]='l'
            elif (constraint=="D"):
                vConstraint[j][i]='g'
            elif (constraint=="L"):
                hConstraint[i][j-1]='l'
            elif (constraint=="R"):
                hConstraint[i][j]='g'
            if (constraint2=="U"):
                vConstraint[j][i-1]='l'
            elif (constraint2=="D"):
                vConstraint[j][i]='g'
            elif (constraint2=="L"):
                hConstraint[i][j-1]='l'
            elif (constraint2=="R"):
                hConstraint[i][j]='g'
print("solve("+printList(init)+","+printList(hConstraint)+","+printList(vConstraint)+",Result).")
input()
