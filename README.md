# Prolog to Pemlog - Adjacent Solver

Final Project for Logic Programming Course 2021

Faculty of Computer Science, University of Indonesia

**Created By - Prolog to Pemlog**:

- Andrew Theodore (1806133862)
- Inigo Ramli (1806133742)
- Ryan Aulia Rachman (1606918534)
- Willy Sandi (1806205786)

## How to Use

For Ubuntu:
- Open Terminal at this repository's directory

For Windows:
- Open unequal.py
- Get game ID URL from https://www.chiark.greenend.org.uk/~sgtatham/puzzles/js/unequal.html
- Type in game ID URL and enter
- Press T if you wish to use Library and F if you do not and press enter


## General Idea
unequal.py will decide which prolog program to run and use library pyswip to run prolog.
ProLog will recursively set value through each cell in the grid, pruning the possible values for each cell. Unpicked values for each row, each column, value of the cell immediately above, and value of the cell immediately to the left are tracked. When a cell is being considered, program will only consider values which:
- Has not been taken by another cell in the same row.
- Has not been taken by another cell in the same row.
- Follows the comparison rule with the cell directly above. If there is no cell above, this rule is skipped.
- Follows the comparison rule with the cell directly to the left. If there is no cell to the left, this rule is skipped.
- Equals to the predetermined value of this cell. If there is no predetermined value, this rule is skipped.
Grids are filled on row-per-row basis, beginning from the topmost row. Rows are filled cell-by-cell starting from the leftmost cell.

## Time Complexity

The solution in essence iterates through all possible Latin Squares of size NxN with additional pruning. Counting the number of all possible Latin Square is classified as [https://oeis.org/A002860](OEIS A002860). Formally, let the number of Latin Squares with size NxN be $L_N$. Therefore, the time complexity of solution presented is $O(|L_N|)$. This value grows quickly and becomes to be computed within reasonable time at N = 7.

