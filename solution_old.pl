in([Head|_],Head).
in([_|Tail],X) :- in(Tail,X).

% exclude(List,ExcludedElement,X) :- X is a list containing every element in List excluding ExcludedElement
% List is assumed to be all unique.
exclude([],_,[]).
exclude([Head|Tail],Head,Tail) :- !.
exclude([Head|Tail],Excluded,[Head|TailX]) :- exclude(Tail,Excluded,TailX).

% permutation(Elements,X) :- X is a permutation of Elements
permutation([],[]).
permutation(Elements,[Element|TailX]) :-
    in(Elements,Element),
    exclude(Elements,Element,NewElements),
    permutation(NewElements,TailX).

% permutation(Masks,Elements,X) :- X is a permutation of Elements with predefined position defined in Masks
permutation([],[],[]).
permutation([x|TailMasks],Elements,[Element|TailX]) :- !,
    in(Elements,Element),
    exclude(Elements,Element,NewElements),
    permutation(TailMasks,NewElements,TailX).
permutation([HeadMask|TailMasks],Elements,[HeadMask|TailX]) :-
    exclude(Elements,HeadMask,NewElements),
    permutation(TailMasks,NewElements,TailX).

% permutation(Constraints,Masks,Elements,X) :- X is a permutation of Elements where
%     - len(Constraints) = len(Masks)-1 = len(Elements)-1 = len(X)-1
%     - If Mask[i] != x, X[i] = Mask[i]
%     - If Constraints[i] = g, X[i] > X[i+1]
%     - If Constraints[i] = l, X[i] < X[i+1]
permutation([],[],[],[]).
permutation([],Masks,Elements,X) :- 
    permutation(Masks,Elements,X).

permutation([x|TailConstraints],[x|TailMasks],Elements,[Element|TailX]) :- !, % Case X
    in(Elements,Element),
    exclude(Elements,Element,NewElements),
    permutation(TailConstraints,TailMasks,NewElements,TailX).
permutation([x|TailConstraints],[HeadMask|TailMasks],Elements,[HeadMask|TailX]) :-
    exclude(Elements,HeadMask,NewElements),
    permutation(TailConstraints,TailMasks,NewElements,TailX).

permutation([l|TailConstraints], [x|TailMasks], Elements, [Element1|[Element2|TailX]]) :- !, % Case G
    in(Elements,Element1),
    in(Elements,Element2),
    Element1 < Element2,
    exclude(Elements,Element1,NewElements),
    permutation(TailConstraints,TailMasks,NewElements,[Element2|TailX]).
permutation([l|TailConstraints], [HeadMask|TailMasks], Elements, [HeadMask|[Element2|TailX]]) :-
    in(Elements,Element2),
    HeadMask < Element2,
    exclude(Elements,HeadMask,NewElements),
    permutation(TailConstraints,TailMasks,NewElements,[Element2|TailX]).


permutation([g|TailConstraints], [x|TailMasks], Elements, [Element1|[Element2|TailX]]) :- !, % Case G
    in(Elements,Element1),
    in(Elements,Element2),
    Element1 > Element2,
    exclude(Elements,Element1,NewElements),
    permutation(TailConstraints,TailMasks,NewElements,[Element2|TailX]).
permutation([g|TailConstraints], [HeadMask|TailMasks], Elements, [HeadMask|[Element2|TailX]]) :-
    in(Elements,Element2),
    HeadMask > Element2,
    exclude(Elements,HeadMask,NewElements),
    permutation(TailConstraints,TailMasks,NewElements,[Element2|TailX]).

% col(M,Col,Res) :- Split M into Col and Res, where Col is the first column of M, and Res is the rest of M.
% Example: If M = [[1,2,3],[4,5,6],[7,8,9]], then
% Col = [1,4,7]
% Res = [[2,3],[5,6],[8,9]]
col([],[],[]) :- !.
col([[HeadRow|TailRow]|TailM],[HeadRow|TailX],[TailRow|TailRes]) :- col(TailM,TailX,TailRes).

% transpose(M,X) :- X is transpose of M, where M is a rectangular 2 dimensional list
transpose([[]|_],[]) :- !.
transpose(M,[FirstCol|TailX]) :- col(M,FirstCol,Res), transpose(Res,TailX).

%
%	Solution:
%	1. Generate all combination where all initial cell values and horizontal constraints are satisfied,
%	2. Transpose solution
%	3. Ensure that all vertical constraints are satisfied as well.
%

% solve(InitialState, Constraints, X) :- X is a solution that satisfies initial state and all horizontal Constraints
solve([],[],_,[]) :- !.
solve([HeadIS|TailIS], [HeadC|TailC],Elements,[HeadX|TailX]) :-
    permutation(HeadC,HeadIS,Elements,HeadX),
    solve(TailIS,TailC,Elements,TailX).

adjacentSolver(InitialState, HorizontalConstraints, VerticalConstraints, Elements, Ans) :-
    transpose(InitialState, IST),
	solve(IST, VerticalConstraints, Elements, AnsT),
	transpose(Ans, AnsT),
    solve(InitialState, HorizontalConstraints, Elements, Ans).