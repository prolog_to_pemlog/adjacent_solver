in([Head|_],Head).
in([_|Tail],X) :- in(Tail,X).

% exclude(List,ExcludedElement,X) :- X is a list containing every element in List excluding ExcludedElement
% List is assumed to be all unique.
exclude([],_,[]).
exclude([Head|Tail],Head,Tail) :- !.
exclude([Head|Tail],Excluded,[Head|TailX]) :- exclude(Tail,Excluded,TailX).


comp(x,_,_).
comp(g,E1,E2) :- E1 > E2.
comp(l,E1,E2) :- E1 < E2.

% solve_row(HorizontalConstraints,
% 			VerticalConstraints,
% 			Vacants,
% 			VerticalVacants,
% 			Left,
%			Up,
% 			Ans,
% 			NewVerticalVacants)

solve_row([],[],[],[],_,_,[],[]).
solve_row([HC|TailHC], [VC|TailVC], V, [VV|TailVV], L, [U|TailU], [M|TailAns], [NVV|TailNVV]) :-
	in(V, M),
	in(VV, M),
	comp(HC, M, L),
	comp(VC, M, U),
	exclude(V, M, NewV),
	exclude(VV, M, NVV),
	solve_row(TailHC, TailVC, NewV, TailVV, M, TailU, TailAns, TailNVV).

% solve_row([x,x,x],[x,x,x],[x,x,x],[1,2,3],[[1,2,3],[1,2,3],[1,2,3]],0,[0,0,0],Ans,NVV).
% solve_row([x,x,x],[x,x,x],[x,1,x],[1,2,3],[[1,2,3],[1,2,3],[1,2,3]],0,[0,0,0],Ans,NVV). Test Mask
% solve_row([x,l,g],[x,x,x],[x,x,x],[1,2,3],[[1,2,3],[1,2,3],[1,2,3]],0,[0,0,0],Ans,NVV). Test HorizontalConstraint
% solve_row([x,l,l],[x,x,x],[x,x,x],[1,2,3],[[1,2,3],[1,2,3],[1,2,3]],0,[0,0,0],Ans,NVV).
% solve_row([x, g, x, x], [x, x, x, x], [x, x, x, x], [1, 2, 3, 4], [[1, 2, 3, 4], [1, 2, 3, 4], [1, 2, 3, 4], [1, 2, 3, 4]], 0, [0, 0, 0, 0], Ans, NVV).

% solve(HorizontalConstraints,
%		VerticalConstraints,
%		HorizontalVacants,
%		VerticalVacants,
%		Up,
%		Ans)
solve([],[],_,_,_,[]).
solve([HC|TailHC], [VC|TailVC], [HV|TailHV], VV, U, [Row|TailAns]) :-
	solve_row(HC, VC, HV, VV, 0, U, Row, NVV),
	solve(TailHC, TailVC, TailHV, NVV, Row, TailAns).

% solve(N,
%		HorizontalConstraints,
%		VerticalConstraints,
%		Mask,
%		Ans)
solve(N,HC,VC,M,Ans) :-
	init_vacant(N, HV),
	init_vacant(N, VV),
	init_row_vacant(N, U),
	Ans = M,
	solve(HC,VC,HV,VV,U,Ans).

% Generate a list containing N list of [1,2,3,...,N]
init_vacant(0,_,[]) :- !.
init_vacant(N, Len, [Row|TailAns]) :- 
	NewN is N-1,
	init_row_vacant(Len, Row),
	init_vacant(NewN, Len, TailAns).
init_vacant(N, Ans) :-
	init_vacant(N, N, Ans).

% Generate a list of [1,2,3,...,N]
init_row_vacant(0, []) :- !.
init_row_vacant(N, [N|TailAns]) :-
	NewN is N-1,
	init_row_vacant(NewN, TailAns).
