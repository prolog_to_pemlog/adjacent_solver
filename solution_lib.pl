:- use_module(library(clpfd)).
:- use_module(library(lists)).
solve(Init,HIneq,VIneq,Result):- %Given Board, Vertical Inequalities symbol, Horizontal Inequalities symbol
    sudokuBox(Init,TempResult),
    %Currently Transposed, so Horizontal is now vertical
    inequalBox(TempResult,VIneq,HIneq,TResult),
    needBruteForce(TResult,Result). %do Brute Force if not solved

needBruteForce(TResult,TResult):-
    isSolved(0,TResult),!.
needBruteForce(Result,Result):-
    outerBruteForce(1,1,Result).

nextXY(X,Y,Result,1,NextY):- % Generate X,Y for next cell
    length(Result,X),!,
    NextY is Y+1.
nextXY(X,Y,_,NextX,Y):-
    NextX is X+1.
    
indexListOfList(List,X,Y,Element):- % List[x][y]
    nth1(X,List,Row),
    nth1(Y,Row,Element).

outerBruteForceDecider(_,_,Result):- % Brute Force termination condition
    isSolved(0,Result),!.
outerBruteForceDecider(X,Y,Result):- % Brute Force on next cell
    outerBruteForce(X,Y,Result).

outerBruteForce(X,Y,Result):- 
    length(Result,N),!,
	bruteForceFrom1ToN(X,Y,N,Result,1),!,
    nextXY(X,Y,Result,NextX,NextY),
    outerBruteForceDecider(NextX,NextY,Result).

bruteForceFrom1ToN(X,Y,_,Result,_):- % skip if element is instantiated
    indexListOfList(Result,X,Y,Element),
    nonvar(Element).
bruteForceFrom1ToN(X,Y,N,Result,CurrentNumber):- %call bruteForce for cell values between 1 and puzzle size
    CurrentNumber=<N,
    bruteForce(X,Y,CurrentNumber,Result),!.
bruteForceFrom1ToN(X,Y,N,Result,CurrentNumber):- %change to nextNumber if above failed
	CurrentNumber<N,
    NextNumber is CurrentNumber+1,
    bruteForceFrom1ToN(X,Y,N,Result,NextNumber).

bruteForce(X,Y,Number,Result):- % When Element=Number result in every blank cell filled
	indexListOfList(Result,X,Y,Element),
    Element=Number,
    isSolved(0,Result),!.
bruteForce(X,Y,Number,Result):- % When Element=Number doesn't result in every blank cell filled
	indexListOfList(Result,X,Y,Element),
    Element=Number,
    nextXY(X,Y,Result,NextX,NextY),
	outerBruteForce(NextX,NextY,Result).

isSolved(Iteration,Result):- %Check if all rows are fully instantiated
    length(Result,Iteration),!.
isSolved(Iteration,Result):-
    nth0(Iteration,Result,Row),
	rowIsSolved(0,Row),
    NextIteration is Iteration+1,
    isSolved(NextIteration,Result).
rowIsSolved(Iteration,Row):- %Check if all cells in row are instantiated
    length(Row,Iteration),!.
rowIsSolved(Iteration,Row):-
    nth0(Iteration,Row,Element),
    nonvar(Element),
    NextIteration is Iteration+1,
    rowIsSolved(NextIteration,Row).

sudokuBox(Rows,Rows) :- % Constraint to a latinBox, similar to sudoku without 3x3 blocks
        length(Rows, N),
        maplist(same_length(Rows), Rows),
        append(Rows, Vs),
        Vs ins 1..N,
        maplist(all_distinct, Rows),
        transpose(Rows, Columns),
        maplist(all_distinct, Columns).

inequalBox(Rows,HRuleTable,VRuleTable,Rows) :- %Constraint according to inequalities signs
    greatOrLessTable(0,VRuleTable,Rows,Rows),
	transpose(Rows,Columns),
    greatOrLessTable(0,HRuleTable,Columns,Columns).


greatOrLessTable(Y,RuleTable,Answer,Answer):- %Base case
    length(RuleTable,Y),!.
greatOrLessTable(Y,RuleTable,AnswerTable,Answer):-%Put each row constraints and iterate on next row
	nth0(Y,RuleTable,RuleRow),
	greatOrLessRow(0,Y,RuleRow,AnswerTable,_),
	NewY is Y+1,
    greatOrLessTable(NewY,RuleTable,AnswerTable,Answer).
greatOrLessRow(X,_,RuleRow,Answer,Answer):- %Base case
	length(RuleRow,X),!.
greatOrLessRow(X,Y,RuleRow,AnswerTable,Result):- %Put constraint on cell, whether (l)ess than or (g)reater than, then continue to next cell in row
    !,nth0(X,RuleRow,Element),
    symbolCheck(Element,X,Y,AnswerTable),
    NewX is X+1,
    greatOrLessRow(NewX,Y,RuleRow,AnswerTable,Result).

symbolCheck(Symbol,X,Y,AnswerTable):- % decide which symbol is written, put inequality constraint
    nonvar(Symbol), Symbol=g,
    !,nth0(Y,AnswerTable,Row),
    nth0(X,Row,A),
    Xb is X+1,
    nth0(Xb,Row,B),
    A#>B,B#>0.
symbolCheck(Symbol,X,Y,AnswerTable):- % decide which symbol is written, put inequality constraint
    nonvar(Symbol), Symbol=l,
    !,nth0(Y,AnswerTable,Row),
    nth0(X,Row,A),
    Xb is X+1,
    nth0(Xb,Row,B),
    A#<B, A#>0.
symbolCheck(_,_,_,_).
