from pyswip import Prolog

#5:0,0L,0R,0,0,0,0,0,0R,0D,0,0,0L,0U,0,0,0,0U,0,0,0,0L,0U,0UR,0,
def printList(array):
    answer="["
    for i in array:
        acc="["
        for j in i:
            acc+=str(j)+","
        acc=acc[:-1]+"],"
        answer+=acc
    return( answer[:-1]+']')
    
def printGrid(grid):
    for i in grid[0]:
        print('|---', end='')
    print('|')
    
    for i in grid:
        for j in i:
            print('| {} '.format(j), end='')
        print('|')
        for j in i:
            print('|---', end='')
        print('|')
    
def printSolutions(ans):
    
    for i in ans:
        printGrid(i['Ans'])
        print('\n')

def setConstraint(i, j, constraint, mode, hConstraint, vConstraint):
    if mode == "T":
        if (constraint=="U"):
            vConstraint[j][i-1]='l'
        elif (constraint=="D"):
            vConstraint[j][i]='g'
        elif (constraint=="L"):
            hConstraint[i][j-1]='l'
        elif (constraint=="R"):
            hConstraint[i][j]='g'
    else:
        if (constraint=="U"):
            vConstraint[i][j]='g'
        elif (constraint=="D"):
            vConstraint[i+1][j]='l'
        elif (constraint=="L"):
            hConstraint[i][j]='g'
        elif (constraint=="R"):
            hConstraint[i][j+1]='l'
        
def getSolutions(mode, length, array):
    init = [['_' for i in range(length) ] for i in range(length)]
    prolog = Prolog()
    if mode == "T":
        prolog.consult("solution_lib.pl")
        hConstraint=[['_' for i in range(length-1) ] for i in range(length)]
        vConstraint=[['_' for i in range(length-1) ] for i in range(length)]
    else:
        prolog.consult("solution.pl")
        n = length
        hConstraint=[['x' for i in range(length) ] for i in range(length)]
        vConstraint=[['x' for i in range(length) ] for i in range(length)]

    for i in range(len(array)):
        for j in range(len(array)):
            element = array[i][j]
            number = element[0]
            constraint = element[1:]
            if (not number=='0'):
                init[i][j]=int(number)
            for k in constraint:
                setConstraint(i, j, k, mode, hConstraint, vConstraint)

    if mode == "T":
        return list(prolog.query("solve({},{},{},Ans)".format(printList(init), printList(hConstraint), printList(vConstraint))))
    else:
        return list(prolog.query("solve({},{},{},{},Ans)".format(length, printList(hConstraint), printList(vConstraint), printList(init))))
        

x=input("URL : ")
x=x.split("chiark.greenend.org.uk/~sgtatham/puzzles/js/unequal.html#")[1][:-1] #Remove url and trailing comma
x=x.split(":")
length=int(x[0])
x=x[1].split(',')
array=[]
for i in range(length):
    array.append(x[i*length:(i+1)*length])

mode = input("Use library? (T/F): ")
solutions = getSolutions(mode, length, array)
if (mode=="T"):
    print("Possible Solution :")
else:
    print("Possible solution(s):")
printSolutions(solutions)

# Agar tidak tutup langsung
input()
